import {Controller, Get, Post, HttpCode} from '@nestjs/common';
import { CatsService } from './cats.service';

@Controller('cats')
export class CatsController{

    constructor (private readonly CatsService: CatsService) {}
    
    @Get()
    findAll(): string {
        return this.CatsService.findAll();
    }

    @Post()
    create(): any
    {
        return 'A new Kitter has been born!'
    }
}